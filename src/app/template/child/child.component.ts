import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  public msgTitle : string;
  constructor() { 
    this.msgTitle = "Child Title";
  }
  ngOnInit() {
  }


  ngOnChanges(){
    console.log('hit on change')
  }

}
