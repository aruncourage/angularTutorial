import { Component, OnInit, ViewChild,ViewChildren,QueryList } from '@angular/core';
import { ChildComponent } from '../child/child.component';
import { ViewChilderenComponent } from '../view-childeren/view-childeren.component'

@Component({
  selector: 'app-component-interaction',
  templateUrl: './component-interaction.component.html',
  styleUrls: ['./component-interaction.component.css']
})
export class ComponentInteractionComponent implements OnInit {

  @ViewChild(ChildComponent)
  private child: ChildComponent;
  constructor() { }

  ngOnInit() {
  }
  public sendMsg(){
    console.log('send msg');
    this.child.msgTitle = "Parent to Child interaction";
  }

}