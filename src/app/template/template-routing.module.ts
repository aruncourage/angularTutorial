import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DisplayingdataComponent } from './displayingdata/displayingdata.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { DirectivesComponent } from './directives/directives.component';
import { PipesComponent } from './pipes/pipes.component';
import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateComponent,
    children:[
      {
        path: '', component: DisplayingdataComponent
      },
      {
        path: 'lifecycle', component: LifecycleComponent
      },
      {
        path: 'interaction', component: ComponentInteractionComponent
      },
      {
        path: 'directives', component: DirectivesComponent
      },
      {
        path: 'pipe', component: PipesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule { }
