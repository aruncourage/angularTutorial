import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChilderenComponent } from './view-childeren.component';

describe('ViewChilderenComponent', () => {
  let component: ViewChilderenComponent;
  let fixture: ComponentFixture<ViewChilderenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChilderenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChilderenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
