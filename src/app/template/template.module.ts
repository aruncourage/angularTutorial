import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateRoutingModule } from './template-routing.module';
import { DisplayingdataComponent } from './displayingdata/displayingdata.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { DirectivesComponent } from './directives/directives.component';
import { PipesComponent } from './pipes/pipes.component';
import { TemplateComponent } from './template/template.component';
import { ChildComponent } from './child/child.component';
import { ViewChilderenComponent } from './view-childeren/view-childeren.component';


@NgModule({
  imports: [
    CommonModule,
    TemplateRoutingModule
  ],
  declarations: [
    DisplayingdataComponent,
    LifecycleComponent,
    ComponentInteractionComponent,
    DirectivesComponent,
    PipesComponent,
    TemplateComponent,
    ChildComponent,
    ViewChilderenComponent
  ]
})
export class TemplateModule { }
