export class TemplateMenus {
    private menus;
    constructor() {
        this.menus = [
            {
                label: 'Directives',
                routerLink: ['/template/directives'],
                title: 'Directives'
            },
            {
                label: 'Component Interaction',
                routerLink: ['/template/interaction'],
                title: 'Component Interaction'
            },
            {
                label: 'Life Cycles',
                routerLink: ['/template/lifecycle'],
                title: 'Life Cycles'

            },
            {
                label: 'Pipes',
                routerLink: ['/template/pipe'],
                title: 'Pipes'
            }
        ];

    }
    getMenu() {
        return this.menus;
    }
}
