import { Component, OnInit } from '@angular/core';
import {TemplateMenus} from './template-menu';
@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  private menuCollections;
  public items:any;
  constructor() { 
    this.menuCollections = new TemplateMenus();
  }

  ngOnInit() {

    this.items = this.menuCollections.getMenu();
    console.dir(this.items);
  }

}
