import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClarityModule } from "@clr/angular";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule}   from '@angular/forms';

import { Routes,RouterModule} from '@angular/router'

import { ConfigService } from './config.service';
import { FormService } from './form.service';

import { AppComponent } from './app.component';
import { ServicesComponent } from './services/services.component';

import { FormComponent } from './form/form.component';
import { QuestionComponent } from './question/question.component';
import { AnswerComponent } from './answer/answer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MultipleoutletComponent } from './multipleoutlet/multipleoutlet.component';
import { AuthGuard } from './routing/auth.guard';

// import { ConfirmPasswordDirective } from './forms-tutorial/confirm-password.directive';

const routes:Routes = [
  { 
    path:'services',
    component:ServicesComponent
  },
  { 
    path:'form',
    component:FormComponent,
    canDeactivate:[AuthGuard]
  },
  {
    path: 'multipleoutlet',
    component: MultipleoutletComponent,
    outlet: 'popup'
  },
  // { 
  //   path:'forms',
  //   loadChildren: 'app/forms-tutorial/forms-tutorial.module#FormsTutorialModule'
  // },
  { 
    path:'routing',
    loadChildren: 'app/routing/routing.module#RoutingModule'
  },
  { 
    path:'template',
    loadChildren: 'app/template/template.module#TemplateModule',
    canLoad: [AuthGuard]
  },
  { 
    path:'',
    component:HomeComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    FormComponent,
    QuestionComponent,
    AnswerComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    MultipleoutletComponent
    // ConfirmPasswordDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    RouterModule.forRoot(
      routes,
      {
        enableTracing: false, // <-- debugging purposes only
      }
    ),
    BrowserAnimationsModule
  ],
  providers: [ConfigService,FormService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
