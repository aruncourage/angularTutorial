import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config.service';
import { FormService } from '../form.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  error: any;
  payLoad:any;
  selectedDropdown:string;
  dataResponse: any;
  selectedId: any;
  questions: any;
  form: FormGroup;
  diriectAccessUrl: boolean;
  constructor(
    private configService: ConfigService, private formService: FormService,private router: Router) {
  }

  ngOnInit() {
    this.redirect();
    this.collectData();
  }
  collectData() {
    let selectedDropdown = '';
    let continiueData = this.configService.continiuePosted;
    if(continiueData.tech!='0'){
      this.selectedId = continiueData.tech
      this.selectedDropdown = 'tech';
    }else if(continiueData.prod!=0){
      this.selectedId = continiueData.prod
      this.selectedDropdown = 'prod';
    }else if(continiueData.gen!=0){
      this.selectedId = continiueData.gen
      this.selectedDropdown = 'gen';
    } 
    this.dataManipulation();
  }

  reset(selectedDropdown, selectedId) {
    if (selectedDropdown == 'tech') {
      this.form.patchValue({
        tech: selectedId
      });
    }
    else if (selectedDropdown == 'prod') {
      this.form.patchValue({
        prod: selectedId
      });
    }
    else if (selectedDropdown == 'gen') {
      this.form.patchValue({
        gen: selectedId
      });
    }

  }
  dataManipulation() {
    this.dataResponse = this.configService.selectedDataResponse;
    var selectedDatas = [];
    var obj = this.configService.continiuePosted;
  
    for (let [key, value] of Object.entries(obj)) {
      if (value === true) {
        for (let entry of this.dataResponse) {
          if (entry.key == key) {
            let newName = {
              key: key,
              label: entry.label,
              value:true,
              controlType:'checkbox',
              type:'checkbox'
            };
            selectedDatas.push(newName);
          }
        }
      }
    }
    this.questions = this.configService.questions.concat(selectedDatas);
    this.form = this.formService.toFormGroup(this.questions);
    this.reset(this.selectedDropdown, this.selectedId)
  }

  redirect() {
    if (this.configService.diriectAccessUrl === false) {
      this.goHome();
    }
  }
  goHome() {
    this.router.navigateByUrl("").then(nav => {
    }, err => {
      console.log(err) // when there's an error
    });
  }
  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
  }

}
