import { Component, Input, OnInit } from '@angular/core';
import { FormGroup }        from '@angular/forms';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
  @Input() question: any;
  @Input() form: FormGroup;
  @Input() selected: string;
  showSelect:boolean;
  constructor() { }

  ngOnInit() {
    this.showSelect = false;
    if(this.question.key == this.selected){
      this.showSelect = true;
    }
    console.log(this.question.key)
    console.log(this.selected);
   

  }
  //get isValid() { return this.form.controls[this.answer.key].valid; }

}
