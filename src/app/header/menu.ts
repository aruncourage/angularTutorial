
export class Menus {
    private menus;
    constructor(){
        this.menus =[
            {
                label: 'Home',
                routerLink: ['/'],
                title:'Home' 
            },
            {
                label: 'Forms', 
                routerLink: ['/form'],
                title:'Forms'
            },
            {
                label: 'Template & DataBinding',
                routerLink: ['/template'],
                title:'Template & DataBinding'
                 
            },
            {
                label: 'HttpClient', 
                routerLink: ['/forms'],
                title:'HttpClient'
            },
            {
                label: 'Routing & Navigation', 
                routerLink: ['/routing'],
                title:'Routing & Navigation'
            },
            {
                label: 'Reactive/ RXjs',
                routerLink: ['/forms'],
                title:'Observable & Rx Js' 
            },
            {
                label: 'Desing Patterns',
                routerLink: ['/forms'],
                title:'Desing Pattern' 
            },
            {
                label: 'Angular Cli & GIT',
                routerLink: ['/forms'],
                title:'Desing Pattern' 
            },
            
        ];
        
    }
    getMenu(){
        return this.menus;
    }
}
