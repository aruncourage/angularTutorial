import { Component, OnInit } from '@angular/core';
import {Menus} from './menu';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private menuCollections;
  public items:any;
  constructor() { 
    this.menuCollections = new Menus();
  }

  ngOnInit() {
    console.dir(this.menuCollections.getMenu());
    this.items = this.menuCollections.getMenu();
  }

}
