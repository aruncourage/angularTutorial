import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
//import { Observable ,throwError} from 'rxjs/Observable';
import { Observable, throwError } from 'rxjs';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { FormGroup } from '@angular/forms';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class ConfigService {
  
  employeesUrl = 'assets/employessResolve.json';
  dynamicUrl = 'assets/technicalform.json';
  techUrl = 'assets/technical.json';
  productUrl = 'assets/product.json';
  generalUrl = 'assets/general.json';
  diriectAccessUrl: boolean = false;
  questions: any;
  selectedDataResponse: any;
  continiuePosted: any;

  constructor(private http: HttpClient) { }
  
  getResolveExample() {
    return this.http.get(this.employeesUrl).pipe(catchError(this.handleError));
  }
  getDynamic() {
    return this.http.get(this.dynamicUrl).pipe(catchError(this.handleError));
  }
/**
 ** @param data:object 
 */
  collectObjects(ticket) {

    if (ticket == "tech") {
      return this.http.get(this.techUrl).pipe(catchError(this.handleError));
    } else if (ticket == "prod") {
      return this.http.get(this.productUrl).pipe(catchError(this.handleError));
    } else if (ticket == "gen") {
      return this.http.get(this.generalUrl).pipe(catchError(this.handleError));
    }

  }
  // private handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }

  //   return new ErrorObservable(
  //     'Something bad happened; please try again later.');
  // };


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/