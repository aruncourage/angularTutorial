import { Component, Input, Output,EventEmitter} from '@angular/core';
import { FormGroup }        from '@angular/forms';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  outputs: ['selectQuestion'],
  styleUrls: ['./question.component.css']
})
export class QuestionComponent {
  @Input() question: any;
  @Input() form: FormGroup;
  selectQuestion: EventEmitter<any> = new EventEmitter<any>();
  
  constructor() { }
  ngOnChanges(changes) {
    if(changes.question !=undefined){
      console.log(changes.question)
    }
    
  }

  displayData(ticket,event){
    let key:string = ticket.key;
    let selectedObj:any = {
      selectedkey:key,
      selectedValue:event.target.value
    }
    this.selectQuestion.emit(selectedObj);
  }
}

