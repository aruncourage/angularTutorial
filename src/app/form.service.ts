import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class FormService {
  constructor() { }

  toFormGroup(questions:any[] ) {
    let group: any = {};

    questions.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', Validators.required): new FormControl(question.value || ''),
      group[question.key] = (question.controlType =='dropdown') ? new FormControl(question.value='0' ): new FormControl(question.value);
    });
    return new FormGroup(group);
  }
}
