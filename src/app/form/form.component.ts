import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormService } from '../form.service';
import { ConfigService } from '../config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  questions: any;
  form: FormGroup;
  payLoad = '';
  error: any;
  answers: any;
  submitButton:boolean;
  constructor(private router: Router, private formService: FormService, private configService: ConfigService) { }

  ngOnInit() {
    this.getSelect();
    this.submitButton = false;
  }
  /**
   * Intitalize or Show the DropDown object.
   */
  getSelect() {
    return this.configService.getDynamic()
      .subscribe(
        data => {
          this.questions = data as string[];
          this.configService.questions = this.questions;
          this.form = this.formService.toFormGroup(this.questions);
        },
        error => this.error = error
      );
  }
  /**
   * @param data:object  (Child Component emitted the Selected Data)
   */
  getRecord(data: any) {
    if(data.selectedValue == '0'){
      this.getSelect();
      this.submitButton = false;
    }else{
      this.reset(data);
      this.submitButton = true;
    }
  }
  /**
   * @param data :object (While on change send the Selected ticket Id and Value)
   */
  reset(data) {
    console.log('reset');
    console.log(data.selectedkey);
    if (data.selectedkey == 'tech') {
      this.rebuild(data.selectedkey);
      setTimeout(() => {
        this.form.patchValue({
          gen: '0',
          prod: '0',
          tech: data.selectedValue
        });
      }, 5);
    }
    else if (data.selectedkey == 'prod') {
      this.rebuild(data.selectedkey);
      setTimeout(() => {
        this.form.patchValue({
          gen: '0',
          prod: data.selectedValue,
          tech: '0'
        });
      }, 5);
    }
    else if (data.selectedkey == 'gen') {
      this.rebuild(data.selectedkey);
      setTimeout(() => {
        this.form.patchValue({
          gen: data.selectedValue,
          prod: '0',
          tech: '0'
        });
      }, 5);
    }

  }
  /** 
   * @param ticket:string (rebuild the Form Based on the Ticket)
   */
  rebuild(ticket){
    console.log('rebuild');
    console.log(ticket);
   return this.configService.collectObjects(ticket)
      .subscribe(
        data => {
          console.log(data);
          this.answers = data['checkbox'];
          this.configService.selectedDataResponse = this.answers;
          this.questions = this.configService.questions
          this.questions = this.questions.concat(this.answers);
          this.form = this.formService.toFormGroup(this.questions);
        },
        error => this.error = error
      );

  }
  onSubmit() {
    let continiueData = this.form.value;
    this.configService.continiuePosted = continiueData;
    this.configService.diriectAccessUrl = true;
    this.payLoad = JSON.stringify(this.form.value);
    this.router.navigateByUrl("/services").then(nav => {
    }, err => {
      console.log(err) 
    });
  }

}





