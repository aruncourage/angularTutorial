import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-resolve',
  templateUrl: './resolve.component.html',
  styleUrls: ['./resolve.component.css']
})
export class ResolveComponent implements OnInit {
  private getData:any
  constructor(private _route:ActivatedRoute) {
    this.getData = this._route.snapshot.data.resovleData
    console.log(this.getData);
    // .subscribe(
    //   data => {
    //     data as string[];
    //     console.log('Arun')
    //     console.log(data);
    //   }
    // );
   }

  ngOnInit() {
  }

}
