import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Resolve,ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import { FormComponent } from '../form/form.component';
import { ConfigService } from '../config.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate,CanActivateChild,CanDeactivate<FormComponent>,CanLoad, Resolve <any> {  
  constructor(private _configService:ConfigService){

  }
  canActivate(): boolean {
    //return confirm('Are you enrolled in Routing Navigation?');
    return true;
  }
  canActivateChild():boolean {
    return true;
    //return confirm('Are you enrolled in Routing  child Navigation?');
  }
  canDeactivate(){
    return confirm('Are you sure you want to navigate from this page?');
  } 
  canLoad():boolean{
    return confirm('Are you sure you want to load this module?');
  }
  resolve(route :ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any>{
    return this._configService.getResolveExample();
  }
}
