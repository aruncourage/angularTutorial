import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigatetocrisisComponent } from './navigatetocrisis.component';

describe('NavigatetocrisisComponent', () => {
  let component: NavigatetocrisisComponent;
  let fixture: ComponentFixture<NavigatetocrisisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigatetocrisisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatetocrisisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
