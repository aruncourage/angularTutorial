import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingComponent } from './routing/routing.component';
import { BasehrefComponent } from './basehref/basehref.component';
import { RouterImportsComponent } from './router-imports/router-imports.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { RouteroutletComponent } from './routeroutlet/routeroutlet.component';
import { RouterstateComponent } from './routerstate/routerstate.component';
import { ActivatedrouteComponent } from './activatedroute/activatedroute.component';
import { RouteguardsComponent } from './routeguards/routeguards.component';
import { CanActivateComponent } from './can-activate/can-activate.component';
import { ComponentlessrouteComponent } from './componentlessroute/componentlessroute.component';
import { CanactivatechildComponent } from './canactivatechild/canactivatechild.component';
import { ResolveComponent } from './resolve/resolve.component';
import { RoutereventsComponent } from './routerevents/routerevents.component';
import { FetchComponent } from './fetch/fetch.component';
import { QueryComponent } from './query/query.component';
import { AsynchronousComponent } from './asynchronous/asynchronous.component';
import { LazyloadingComponent } from './lazyloading/lazyloading.component';
import { CanloadguardComponent } from './canloadguard/canloadguard.component';
import { PreloadingComponent } from './preloading/preloading.component';
import { CustompreloadingComponent } from './custompreloading/custompreloading.component';
import { InspectComponent } from './inspect/inspect.component';
import { ChildroutingComponent } from './childrouting/childrouting.component';
import { RelativeComponent } from './relative/relative.component';
import { NavigatetocrisisComponent } from './navigatetocrisis/navigatetocrisis.component';
import { MultipleComponent } from './multiple/multiple.component';
import { RouteeventComponent } from './routeevent/routeevent.component';
import { WildcardComponent } from './wildcard/wildcard.component';
import { AuthGuard} from './auth.guard';
const routes: Routes = [
   {
    path: '',
    component: RoutingComponent,
    canActivate :[AuthGuard],
    canActivateChild:[AuthGuard],
    children: [
      {
        path: 'basehref', component: BasehrefComponent
      },
      {
        path: 'routeimports', component: RouterImportsComponent
      },
      {
        path: 'configuration', component: ConfigurationComponent
      },
      {
        path: 'routeroutlet', component: RouteroutletComponent
      },
      {
        path: 'routerstate/:id', component: RouterstateComponent
      },
      {
        path: 'routeevents', component: RouteeventComponent
      },
      {
        path: 'activatedroute', component: ActivatedrouteComponent
      },
      {
        path: 'routeguards', component: RouteguardsComponent
      },
      {
        path: 'canActivate', component: CanActivateComponent
      },
      {
        path: 'componentlessroute', component: ComponentlessrouteComponent
      },
      {
        path: 'canactivatechild', component: CanactivatechildComponent,canActivateChild: [AuthGuard]
      },
      {
        path: 'resolve', 
        component: ResolveComponent,
        resolve:{
          resovleData:AuthGuard
        }
      },
      {
        path: 'routerevents', component: RoutereventsComponent
      },
      {
        path: 'configuration', component: ConfigurationComponent
      },
      {
        path: 'fetch', component: FetchComponent
      },
      {
        path: 'query', component: QueryComponent
      },
      {
        path: 'asynchronous', component: AsynchronousComponent
      },
      {
        path: 'canloadguard', component: CanloadguardComponent
      },
      {
        path: 'asynchronous', component: AsynchronousComponent
      },
      {
        path: 'preloading', component: PreloadingComponent
      },
      {
        path: 'custompreloading', component: CustompreloadingComponent
      },
      {
        path: 'inspect', component: InspectComponent
      },
      {
        path: 'childrouting', component: ChildroutingComponent
      },
      {
        path: 'custompreloading', component: CustompreloadingComponent
      },
      {
        path: 'navigatetocrisis', component: NavigatetocrisisComponent
      },
      {
        path: 'multiple', component: MultipleComponent
      },
      {
        path: 'lazyloading', component: LazyloadingComponent
      },
      {
        path: 'relative', component: RelativeComponent
      },
      { path: '', component: RelativeComponent },
      { path: '**', component: WildcardComponent }
    ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutingRoutingModule { }
