import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteeventComponent } from './routeevent.component';

describe('RouteeventComponent', () => {
  let component: RouteeventComponent;
  let fixture: ComponentFixture<RouteeventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteeventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteeventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
