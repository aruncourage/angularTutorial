import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterstateComponent } from './routerstate.component';

describe('RouterstateComponent', () => {
  let component: RouterstateComponent;
  let fixture: ComponentFixture<RouterstateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouterstateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterstateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
