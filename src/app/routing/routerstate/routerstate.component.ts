import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-routerstate',
  templateUrl: './routerstate.component.html',
  styleUrls: ['./routerstate.component.css']
})
export class RouterstateComponent implements OnInit {
  sub: any;
  id:any;
  subId: any;
  fragment:any
  idValue:any;
  parenData:any;
  frag:any;
  fragvalue:any;
  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.parenData = this.route.parent.url;

    
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      console.log(this.id);
    });
    // this.frag =this.route.fragment.subscribe((fragment: string) => {
    //   console.log("My hash fragment is here => ", fragment)
    //   this.fragvalue = fragment;
    //   console.log('pushkal', this.fragvalue);
    // })
    this.subId = this.route.params.subscribe(queryParams => {
      this.idValue = +queryParams['id'];
      console.log(this.idValue);
    });
    const id: string = this.route.snapshot.params.id;
    const url: string = this.route.snapshot.url.join('');
  }

}


