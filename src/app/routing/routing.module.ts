import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingRoutingModule } from './routing-routing.module';
import { RoutingComponent } from './routing/routing.component';
import { BasehrefComponent } from './basehref/basehref.component';
import { RouterImportsComponent } from './router-imports/router-imports.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { RouteroutletComponent } from './routeroutlet/routeroutlet.component';
import { RouterstateComponent } from './routerstate/routerstate.component';
import { ActivatedrouteComponent } from './activatedroute/activatedroute.component';
import { RouteguardsComponent } from './routeguards/routeguards.component';
import { CanActivateComponent } from './can-activate/can-activate.component';
import { ComponentlessrouteComponent } from './componentlessroute/componentlessroute.component';
import { CanactivatechildComponent } from './canactivatechild/canactivatechild.component';
import { ResolveComponent } from './resolve/resolve.component';
import { RoutereventsComponent } from './routerevents/routerevents.component';
import { FetchComponent } from './fetch/fetch.component';
import { QueryComponent } from './query/query.component';
import { AsynchronousComponent } from './asynchronous/asynchronous.component';
import { LazyloadingComponent } from './lazyloading/lazyloading.component';
import { CanloadguardComponent } from './canloadguard/canloadguard.component';
import { PreloadingComponent } from './preloading/preloading.component';
import { CustompreloadingComponent } from './custompreloading/custompreloading.component';
import { InspectComponent } from './inspect/inspect.component';
import { ChildroutingComponent } from './childrouting/childrouting.component';
import { RelativeComponent } from './relative/relative.component';
import { NavigatetocrisisComponent } from './navigatetocrisis/navigatetocrisis.component';
import { MultipleComponent } from './multiple/multiple.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RouteeventComponent } from './routeevent/routeevent.component';
import { WildcardComponent } from './wildcard/wildcard.component';
import { AuthGuard} from './auth.guard';
@NgModule({
  imports: [
    CommonModule,
    RoutingRoutingModule
  ],
  providers:[AuthGuard],
  declarations: [
    RoutingComponent,
    BasehrefComponent,
    RouterImportsComponent,
    ConfigurationComponent,
    RouteroutletComponent,
    RouterstateComponent,
    ActivatedrouteComponent,
    RouteguardsComponent,
    CanActivateComponent,
    ComponentlessrouteComponent,
    CanactivatechildComponent,
    ResolveComponent,
    RoutereventsComponent,
    FetchComponent,
    QueryComponent,
    AsynchronousComponent,
    LazyloadingComponent,
    CanloadguardComponent,
    PreloadingComponent,
    CustompreloadingComponent,
    InspectComponent,
    ChildroutingComponent,
    RelativeComponent,
    NavigatetocrisisComponent,
    MultipleComponent,
    PagenotfoundComponent,
    RouteeventComponent,
    WildcardComponent
  ]
})
export class RoutingModule { }
