import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteguardsComponent } from './routeguards.component';

describe('RouteguardsComponent', () => {
  let component: RouteguardsComponent;
  let fixture: ComponentFixture<RouteguardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteguardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteguardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
