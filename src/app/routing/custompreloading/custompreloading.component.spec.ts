import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustompreloadingComponent } from './custompreloading.component';

describe('CustompreloadingComponent', () => {
  let component: CustompreloadingComponent;
  let fixture: ComponentFixture<CustompreloadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustompreloadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustompreloadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
