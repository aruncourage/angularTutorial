import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasehrefComponent } from './basehref.component';

describe('BasehrefComponent', () => {
  let component: BasehrefComponent;
  let fixture: ComponentFixture<BasehrefComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasehrefComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasehrefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
