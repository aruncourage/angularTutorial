import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanloadguardComponent } from './canloadguard.component';

describe('CanloadguardComponent', () => {
  let component: CanloadguardComponent;
  let fixture: ComponentFixture<CanloadguardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanloadguardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanloadguardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
