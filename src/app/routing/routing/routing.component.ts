import { Component, OnInit } from '@angular/core';
import {RoutingMenus} from './routing-menu';
@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.scss']
})
export class RoutingComponent implements OnInit {
  private menuCollections;
  public items:any;
  constructor() {
    this.menuCollections = new RoutingMenus();
   }

  ngOnInit() {
   
    this.items = this.menuCollections.getMenu();
    console.dir(this.items);
  }

}
