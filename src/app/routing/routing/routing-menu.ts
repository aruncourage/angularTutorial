export class RoutingMenus {
    private menus;
    constructor(){
        this.menus =[
            {
                label: 'Routing navigation',
                routerLink: ['/routing/relative'],
                title:'Relative navigation' 
            },
            {
                label: 'Base Href',
                routerLink: ['/routing/basehref'],
                title:'Base Href' 
            },
            {
                label: 'Router Imports', 
                routerLink: ['/routing/routeimports'],
                title:'Router Imports'
            },
            {
                label: 'Configuration',
                routerLink: ['/routing/configuration'],
                title:'Configuration'
                 
            },
            {
                label: 'Router Outlet', 
                routerLink: ['/routing/routeroutlet'],
                title:'Router Outlet'
            },
            {
                label: 'Router State', 
                routerLink: ['/routing/routerstate/10'],
                title:'Router State'
            },
            {
                label: 'Route Events',
                routerLink: ['/routing/routeevents'],
                title:'Route Events' 
            },
            {
                label: 'Activated Route', 
                routerLink: ['/routing/activatedroute'],
                title:'Activated Route'
            },
            {
                label: 'Wild Card / Page Not Found',
                routerLink: ['/routing/wildcard'],
                title:'Wild Card ' 
            },
            {
                label: 'Child Routing',
                routerLink: ['/routing/childrouting'],
                title:'Child Route' 
            },
            {
                label: 'Route Guards',
                routerLink: ['/routing/routeguards'],
                title:'Route Guards' 
            },
            {
                label: 'CanActivate',
                routerLink: ['/routing/canActivate'],
                title:'CanActivate' 
            },
            {
                label: 'CanLoad Guard',
                routerLink: ['/routing/canloadguard'],
                title:'CanLoad Guard' 
            },
            
            {
                label: 'CanActivate Child',
                routerLink: ['/routing/canactivatechild'],
                title:'CanActivate Child' 
            },
            {
                label: 'CanDeactivate',
                routerLink: ['/routing/canactive'],
                title:'Can Deactivate' 
            },
            {
                label: 'Resolve',
                routerLink: ['/routing/resolve'],
                title:'Resolve' 
            },
            {
                label: 'Component Less Route',
                routerLink: ['/routing/componentlessroute'],
                title:'Component Less Route' 
            },
            {
                label: 'Asynchronous routing',
                routerLink: ['/routing/asynchronous'],
                title:'Asynchronous routing' 
            },
            {
                label: 'Lazy Loading route configuration',
                routerLink: ['/routing/lazyloading'],
                title:'Lazy Loading route configuration' 
            },
            {
                label: 'Custom Preloading Strategy',
                routerLink: ['/routing/custompreloading'],
                title:'Custom Preloading Strategy' 
            },
            {
                label: 'Inspect the routers',
                routerLink: ['/routing/inspect'],
                title:'Inspect the routers' 
            },
            
            {
                label: 'Displaying multiple routes in named outlets',
                routerLink: ['/routing/multiple'],
                title:'Displaying multiple routes in named outlets' 
            }
        ];
        
    }
    getMenu(){
        return this.menus;
    }
}
