import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterImportsComponent } from './router-imports.component';

describe('RouterImportsComponent', () => {
  let component: RouterImportsComponent;
  let fixture: ComponentFixture<RouterImportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouterImportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterImportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
