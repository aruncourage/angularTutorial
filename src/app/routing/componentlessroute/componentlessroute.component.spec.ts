import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentlessrouteComponent } from './componentlessroute.component';

describe('ComponentlessrouteComponent', () => {
  let component: ComponentlessrouteComponent;
  let fixture: ComponentFixture<ComponentlessrouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentlessrouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentlessrouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
